Source: pentaho-reporting-flow-engine
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Rene Engelhard <rene@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk-headless,
               ant,
               libbsh-java,
               libjcommon-java,
               libitext-java,
               libjakarta-poi-java,
               libpixie-java,
               liblog4j1.2-java,
               libserializer-java,
               libformula-java,
               libflute-java,
               libfonts-java,
               liblayout-java,
               libloader-java,
               librepository-java,
               libxml-java,
               libsac-java,
               docbook-utils,
               docbook-xml,
               libbase-java,
               libcommons-logging-java,
               javahelper
Standards-Version: 4.7.1
Vcs-Browser: https://salsa.debian.org/java-team/pentaho-reporting-flow-engine
Vcs-Git: https://salsa.debian.org/java-team/pentaho-reporting-flow-engine.git
Homepage: http://www.pentaho.com/

Package: libpentaho-reporting-flow-engine-java
Architecture: all
Section: java
Depends: ${java:Depends},
         ${misc:Depends}
Suggests: libjfreereport-java-doc
#Depends: libjcommon-java (>= 1.0.10), libitext-java, libjakarta-poi-java, liblog4j1.2-java, libserializer-java, libformula-java, libflute-java, libfonts-java (>= 0.3.2), liblayout-java, libloader-java, librepository-java, libxml-java (>= 0.8.9), libsac-java
Conflicts: libjfreereport-java
Provides: libjfreereport-java
Replaces: libjfreereport-java
Description: report library for java
 Pentaho Reporting Flow Engine is a free Java report library.
 .
 It has the following features:
    * full on-screen print preview;
    * data obtained via Swing's TableModel interface
    (making it easy to print data directly from your application);
    * XML-based report definitions;
    * output to the screen, printer or various export formats
    (PDF, HTML, CSV, Excel, plain text);
    * support for servlets (uses the JFreeReport extensions)
    * extensive documentation in Acrobat PDF format, plus full Javadocs;

Package: libpentaho-reporting-flow-engine-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Conflicts: libjfreereport-java-doc
Provides: libjfreereport-java-doc
Replaces: libjfreereport-java-doc
Description: report library for java documentation
 Pentaho Reporting Flow Engine is a free Java report library.
 .
 It has the following features:
    * full on-screen print preview;
    * data obtained via Swing's TableModel interface
    (making it easy to print data directly from your application);
    * XML-based report definitions;
    * output to the screen, printer or various export formats
    (PDF, HTML, CSV, Excel, plain text);
    * support for servlets (uses the JFreeReport extensions)
    * extensive documentation in Acrobat PDF format, plus full Javadocs;
 .
 This package contains the Javadoc, the Manual and the Developers Guide of
 the Pentaho Reporting Flow Engine.
